# Skill Learning in Internally-Guided sequencing

The repository contains code and data for the empirical and computational studies presented in the thesis **Bera K. (2021). _An Empirical and Computational Investigation of Skill Learning in Internally-guided Sequencing_. IIIT-Hyderabad, India**.


Experimental code, analysis scripts and preprocessed data coming soon.
